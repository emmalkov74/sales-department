import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:9090/api/rest",
  headers: {
    "Content-type": "application/json",
    "Access-Control-Allow-Origin": "https://localhost:8080",
    "Access-Control-Allow-Credentials": "true",
  }
});


