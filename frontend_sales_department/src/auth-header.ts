import {useAuthUserAppStore} from "@/store/app";

export default function authHeader() {
  const user = useAuthUserAppStore().getAuthUser
  if (user) {
    if (user && user.userToken) {
      return { Authorization: 'Bearer ' + user.userToken };
    } else {
      return {};
    }
  }



}
