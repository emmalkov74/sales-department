import http from "../http-common";
import Company from "@/models/Company";
import authHeader from "@/auth-header";

class CompanyDataService{

  getAll() {
    return  http.get("/companies/getAll", { headers: authHeader() });
  }

  get(id: any) {
    return http.get(`/companies/getOneById?id=${id}`, { headers: authHeader() });
  }

  create(data: Company) {
    return http.post("/companies/add", data, { headers: authHeader() });
  }

  search(company: Company){
    return http.post("/companies/search", company, { headers: authHeader() });
  }

  update(data: Company) {
    const id = data.id
    return http.put(`/companies/update?id=${id}`, data, { headers: authHeader()});
  }

  // delete(id) {
  //   return http.delete(`/tutorials/${id}`);
  // }

  // deleteAll() {
  //   return http.delete(`/tutorials`);
  // }

  // findByTitle(title) {
  //   return http.get(`/tutorials?title=${title}`);
  // }
}

export default new CompanyDataService();
