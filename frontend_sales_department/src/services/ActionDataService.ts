import http from "../http-common";
import authHeader from "@/auth-header";
import Action from "@/models/Action";

class ActionDataService {
  getAll() {
    return http.get("/actions/getAll", { headers: authHeader() });
  }

  get(id: any) {
    return http.get(`/actions/getOneById?id=${id}`,{ headers: authHeader() });
  }

  create(data: Action) {
    return http.post("/actions/add", data, { headers: authHeader() });
  }

  update(data: Action) {
    const id = data.id
    return http.put(`/actions/update?id=${id}`, data, { headers: authHeader()});
  }

  delete(id: any) {
    return http.delete(`/actions/delete/${id}`,{ headers: authHeader()});
  }

  // deleteAll() {
  //   return http.delete(`/tutorials`);
  // }

  // findByTitle(title) {
  //   return http.get(`/tutorials?title=${title}`);
  // }

  findAllByCompanyId(id: any) {
    return http.get(`/actions/getAllByCompanyId?id=${id}`,{ headers: authHeader() });
  }
}

export default new ActionDataService();
