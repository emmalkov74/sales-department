import http from "../http-common";
import Client from "@/models/Client";
import authHeader from "@/auth-header";

class ClientDataService {
  getAll() {
    return http.get("/clients/getAll", { headers: authHeader() });
  }

  get(id: any) {
    return http.get(`/clients/getOneById?id=${id}`,{ headers: authHeader() });
  }

  create(data: Client) {
    return http.post("/clients/add", data, { headers: authHeader() });
  }

  update(id: any, data: Client) {
    return http.put(`/clients/update?id=${id}`, data, { headers: authHeader()});
  }

  delete(id: any) {
    return http.delete(`/clients/delete/${id}`,{ headers: authHeader()});
  }

  // deleteAll() {
  //   return http.delete(`/tutorials`);
  // }

  // findByTitle(title) {
  //   return http.get(`/tutorials?title=${title}`);
  // }

  findAllByCompanyId(id: any) {
    return http.get(`clients/getAllByCompanyId?id=${id}`,{ headers: authHeader() });
  }
}

export default new ClientDataService();
