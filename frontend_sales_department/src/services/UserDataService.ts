import http from "../http-common";
import authHeader from "@/auth-header";
import User from "@/models/User";
import LoginDTO from "@/models/LoginDTO";

class UserDataService {
  getAll() {
    return http.get("/users/getAll", { headers: authHeader() });
  }

  get(id: any) {
    return http.get(`/users/getOneById?id=${id}`,{ headers: authHeader() });
  }

  create(data: User) {
    return http.post("/users/add", data,{ headers: authHeader() });
  }

  update(id: any, data: User) {
    return http.put(`/users/update?id=${id}`, data, { headers: authHeader()});
  }

  delete(id: any) {
    return http.delete(`/users/delete/${id}`,{ headers: authHeader()});
  }

  // deleteAll() {
  //   return http.delete(`/tutorials`);
  // }

  findByLogin(login: string) {
    return http.get(`/users/getOneByLogin?login=${login}`);
  }

  findByEmail(email: string){
    return http.get(`/users/getOneByEmail?email=${email}`, { headers: authHeader()})
  }
  isEmail(email: string){
    return http.get(`/users/getIsEmail?email=${email}`)
  }

  changePassword(data: LoginDTO) {
    return http.post("/users/change-password", data)
  }
}

export default new UserDataService();
