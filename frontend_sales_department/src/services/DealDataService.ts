import http from "@/http-common";
import authHeader from "@/auth-header";
import Deal from "@/models/Deal";
import Action from "@/models/Action";

class DealDataService{
  findAllByCompanyId(id: any) {
    return http.get(`/deals/getAllByCompanyId?id=${id}`,{ headers: authHeader() });
  }

  create(data: Deal) {
    return http.post("/deals/add", data, { headers: authHeader() });
  }

  get(id: any) {
    return http.get(`/deals/getOneById?id=${id}`,{ headers: authHeader() });
  }

  update(data: Deal) {
    const id = data.id
    return http.put(`/deals/update?id=${id}`, data, { headers: authHeader()});
  }

  delete(id: any) {
    return http.delete(`/deals/delete/${id}`,{ headers: authHeader()});
  }
}
export default new DealDataService()
