import http from "../http-common";
import LoginDTO from "@/models/LoginDTO";

class AuthService {
  login(loginDTOUser: LoginDTO) {
    const user = {
      login: loginDTOUser.login,
      password: loginDTOUser.password
    }
    const serializedUser = JSON.stringify(user);
    return http
      .post('/users/auth', serializedUser)
      .then(response => {
          return response.data
      });
  }

}
export default new AuthService()
