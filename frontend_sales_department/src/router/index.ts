import { createRouter, createWebHistory } from 'vue-router'
import * as path from "path";

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'Home',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
      },
      {
        path: '/clients',
        props: true,
        name: 'Clients',
        component: () => import('@/components/clients/Clients.vue')
      },
      {
        path: '/updateClient/:id',
        props: true,
        name: 'UpdateClient',
        component: () => import('@/components/clients/UpdateClient.vue')
      },
      {
        path: '/addClient/:companyId',
        props: true,
        name: 'AddClient',
        component: () => import('@/components/clients/AddClient.vue')
      },
      {
        path: '/companies',
        name: 'Companies',
        component: () => import('@/components/companies/Companies.vue')
      },
      {
        path: '/company/:id',
        props: true,
        name: 'Company',
        component: () => import('@/components/companies/Company.vue')
      },
      {
        path: '/addCompany',
        name: 'AddCompany',
        component: () => import('@/components/companies/AddCompany.vue')
      },
      {
        path: '/updateCompany/:id',
        props: true,
        name: 'UpdateCompany',
        component: () => import('@/components/companies/UpdateCompany.vue')
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('@/components/users/Users.vue')
      },
      {
        path: '/addUser',
        name: 'AddUser',
        component: () => import('@/components/users/addUser.vue')
      },
      {
        path: '/updateUser/:id',
        props: true,
        name: 'UpdateUser',
        component: () => import('@/components/users/updateUser.vue')
      },
      {
        path: '/actions',
        name: 'Actions',
        component: () => import('@/components/actions/Actions.vue')
      },
      {
        path: '/addAction/:companyId',
        props: true,
        name: 'AddAction',
        component: () => import('@/components/actions/AddAction.vue')
      },
      {
        path: '/action/:id',
        props: true,
        name: 'Action',
        component: () => import('@/components/actions/Action.vue')
      },
      {
        path: '/addDeal/:companyId',
        props: true,
        name: 'AddDeal',
        component: () => import('@/components/deals/AddDeal.vue')
      },
      {
        path: '/deal/:id',
        props: true,
        name: 'Deal',
        component: () => import('@/components/deals/Deal.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/components/Login.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
