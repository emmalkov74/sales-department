class Message{
  text: string;
  color: string;
  show: boolean;

  constructor( text: string, color: string, show: boolean) {
    this.text = text;
    this.color = color;
    this.show = show;
  }
}
export default Message;
