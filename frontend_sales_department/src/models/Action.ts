import GenericModel from "@/models/GenericModel";

class Action extends GenericModel{
  actionDate: string;
  title: string;
  description: string;
  isDone: boolean;
  userId: number;
  companyId: number

  constructor(id: number, createdWhen: string, createdBy: string, actionDate: string,
              title: string, description: string, isDone: boolean, userId: number, companyId: number) {
    super(id, createdWhen, createdBy)
    this.actionDate = actionDate;
    this.title = title;
    this.description = description;
    this.isDone = isDone;
    this.userId = userId;
    this.companyId = companyId;
  }
}
export default Action;






