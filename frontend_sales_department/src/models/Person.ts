import GenericModel from "@/models/GenericModel";

class Person extends GenericModel{
    fio: string;
    phone: string;
    email: string;
    position: string;

    constructor(id: number, createdWhen: string, createdBy: string,fio: string, phone: string,  email: string, position: string) {
      super(id, createdWhen, createdBy);
      this.fio = fio;
      this.phone = phone;
      this.email = email;
      this.position = position;
    }


}
export default Person;
