import GenericModel from "@/models/GenericModel";

class Deal extends GenericModel{
  dealDate: string;
  dealAmount: number;
  isDone: boolean;
  userId: number;
  companyId: number;


  constructor(id: number, createdWhen: string, createdBy: string,
              dealDate: string, dealAmount: number, isDone: boolean, userId: number, companyId: number) {
    super(id, createdWhen, createdBy)
    this.dealDate = dealDate;
    this.dealAmount = dealAmount;
    this.isDone = isDone;
    this.userId = userId;
    this.companyId = companyId;
  }
}
export default Deal
