class AuthUser{
  private _userRole: string
  private _userToken: string
  private _userName: string


  constructor(userRole: string, userToken: string, userName: string) {
    this._userRole = userRole;
    this._userToken = userToken;
    this._userName = userName;
  }

  get userRole(): string {
    return this._userRole;
  }

  get userToken(): string {
    return this._userToken;
  }

  get userName(): string {
    return this._userName;
  }
}
export default AuthUser
