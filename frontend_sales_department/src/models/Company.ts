import GenericModel from "@/models/GenericModel";

class Company extends GenericModel{
  name: string;
  inn: string;
  address: string;
  clientsIds: number[];
  dealsIds: number[];
  actionsIds: number[];

  constructor(id: number, createdWhen: string, createdBy: string,name: string, inn: string,
              address: string, clientsIds: number[], dealsIds: number[], actionsIds: number[]) {
    super(id, createdWhen, createdBy)
    this.name = name;
    this.inn = inn;
    this.address = address;
    this.clientsIds = clientsIds;
    this.dealsIds = dealsIds;
    this.actionsIds = actionsIds;
  }
}
export default Company;
