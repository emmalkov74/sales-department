import Person from "@/models/Person";


class Client extends Person{

    companyId: number;

    constructor(id: number, createdWhen: string, createdBy: string,
                fio: string, phone: string,  email: string, position: string, companyId: number) {
        super(id, createdWhen, createdBy,fio, phone,  email, position);
        this.companyId = companyId;
    }

}
export default Client;
