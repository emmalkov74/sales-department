
class LoginDTO {
  login: string
  password: string

  kod: string

  constructor(login: string, password: string, kod: string) {
    this.login = login;
    this.password = password;
    this.kod = kod;
  }
}
export default LoginDTO;
