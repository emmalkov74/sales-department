class GenericModel{
  id: number;
  createdWhen: string;
  createdBy: string;

  constructor(id: number, createdWhen: string, createdBy: string) {
    this.id = id;
    this.createdWhen = createdWhen;
    this.createdBy = createdBy;
  }
}
export default GenericModel

