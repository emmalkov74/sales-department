import Person from "@/models/Person";
import Roles from "@/Roles";

class User extends Person{
    login: string;
    password: string;
    role: Roles;


    constructor(id: number, createdWhen: string, createdBy: string,fio: string, phone: string,  email: string, position: string, login: string, password: string, role: Roles) {
        super(id, createdWhen, createdBy,fio, phone, email, position);
        this.login = login;
        this.password = password;
        this.role = role;
    }

}
export default User;
