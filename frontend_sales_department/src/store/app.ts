// Utilities
import { defineStore } from 'pinia'
import Message from "@/models/Message";
import AuthUser from "@/models/AuthUser";
import User from "@/models/User";

export const useMessageAppStore = defineStore('messageStore', {
  state: () => ({
    message: new Message('', '', false),
  }),
  actions: {
    changeState(params: Message) {
      // console.log ('The receiving parameters ===>', params)
      this.message = params
    }
  }
})

  export const useAuthUserAppStore = defineStore('authUserStore', {
    state: () => ({
      authUser: null as AuthUser | null,
      user: null as User | null
    }),
    getters: {
      getAuthUser: (state) => {
        if(state.authUser){
          return state.authUser
        }
        return null
      },
      getUser: (state) => {
        if(state.user){
          return state.user
        }
        return null
      },
    },
    actions: {
      setAuthUser(user: AuthUser | null) {
        this.authUser = user
      },
      setUser(user: User | null) {
        this.user = user
      },

    }

})


