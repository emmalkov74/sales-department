package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.config.MailConstants;
import com.example.backendsalesdepartment.dto.LoginDTO;
import com.example.backendsalesdepartment.dto.UserDTO;
import com.example.backendsalesdepartment.mapper.UserMapper;
import com.example.backendsalesdepartment.models.User;
import com.example.backendsalesdepartment.repository.UserRepository;
import com.example.backendsalesdepartment.utils.MailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.JavaMailSender;
import org.webjars.NotFoundException;

@Service
@Slf4j
public class UserService
        extends GenericService<User, UserDTO>
{
    //  Инжектим конкретный репозиторий для работы с таблицей books
    private final UserRepository repository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final JavaMailSender javaMailSender;


    protected UserService(UserRepository repository,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          UserMapper mapper,
                          JavaMailSender javaMailSender) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public UserDTO create(UserDTO object) {
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }


    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    public UserDTO getOneByLogin(String login) {
        return  mapper.toDto(repository.findUserByLogin(login));
    }

    public UserDTO getOneByEmail(String email) {
        return  mapper.toDto(repository.findUserByEmail(email));
    }

    public Boolean isEmail(String email) {
        UserDTO userDTO = mapper.toDto(repository.findUserByEmail(email));
        if(userDTO.getEmail() != null){
            String randomNumber = (int)(Math.random() * 10000) + "";
            userDTO.setChangePasswordToken(bCryptPasswordEncoder.encode(randomNumber));
            repository.save(mapper.toEntity(userDTO));
            SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
                    MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                    MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + userDTO.getChangePasswordToken());
            javaMailSender.send(mailMessage);
            return true;
        }
        return false;
    }
    public UserDTO changePassword(LoginDTO loginDTO) {

        UserDTO userDTO = mapper.toDto(repository.findUserByChangePasswordToken(loginDTO.getKod()));
        log.info("UserDTO {}",userDTO.toString());
        if(userDTO.getPassword() != null){
            userDTO.setPassword(bCryptPasswordEncoder.encode(loginDTO.getPassword()));
            userDTO.setChangePasswordToken(null);
            update(userDTO);
            return userDTO;
        }
        return null;
    }


}
