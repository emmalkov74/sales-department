package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.CompanyDTO;
import com.example.backendsalesdepartment.mapper.CompanyMapper;
import com.example.backendsalesdepartment.models.Company;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService extends GenericService<Company, CompanyDTO> {

    private CompanyRepository companyRepository;
    protected CompanyService(CompanyRepository companyRepository,
                             CompanyMapper companyMapper) {
        super(companyRepository, companyMapper);
        this.companyRepository = companyRepository;
    }


    public List<CompanyDTO> search(CompanyDTO companyDTO) {
        return mapper.toDTOs(companyRepository.searchCompanies(
                companyDTO.getName(),
                companyDTO.getInn(),
                companyDTO.getAddress(),
                companyDTO.getCreatedBy()
                ));
    }
}

