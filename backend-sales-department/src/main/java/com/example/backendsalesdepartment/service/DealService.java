package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.DealDTO;
import com.example.backendsalesdepartment.mapper.DealMapper;
import com.example.backendsalesdepartment.models.Deal;
import com.example.backendsalesdepartment.repository.DealRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DealService extends GenericService<Deal, DealDTO> {
    private DealRepository dealRepository;

    protected DealService(DealRepository dealRepository, DealMapper dealMapper) {
        super(dealRepository, dealMapper);
        this.dealRepository = dealRepository;
    }

    public List<DealDTO> getAllByCompanyID(Long id) {
        return mapper.toDTOs(dealRepository.findAllByCompanyId(id));
    }
}
