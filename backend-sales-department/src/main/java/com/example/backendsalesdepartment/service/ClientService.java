package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.mapper.ClientMapper;
import com.example.backendsalesdepartment.mapper.GenericMapper;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.repository.ClientRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ClientService extends GenericService<Client, ClientDTO> {
    private ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository,
                         ClientMapper clientMapper) {
        super(clientRepository, clientMapper);
        this.clientRepository = clientRepository;
    }

    public List<ClientDTO> getAllByCompanyID(Long id) {
        return mapper.toDTOs(clientRepository.findAllByCompanyId(id));
    }

}
