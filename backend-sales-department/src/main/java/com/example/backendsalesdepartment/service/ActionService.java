package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.mapper.ActionMapper;
import com.example.backendsalesdepartment.mapper.GenericMapper;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.repository.ActionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionService extends GenericService<Action, ActionDTO>{
    private ActionRepository actionRepository;

    public ActionService(ActionRepository actionRepository,
                         ActionMapper actionMapper) {
        super(actionRepository, actionMapper);
        this.actionRepository = actionRepository;
    }

    public List<ActionDTO> getAllByCompanyID(Long id) {
        return mapper.toDTOs(actionRepository.findAllByCompanyId(id));
    }
}
