package com.example.backendsalesdepartment.config.jwt;

import com.example.backendsalesdepartment.config.RestAuthenticationEntryPoint;
import com.example.backendsalesdepartment.service.userdetails.CustomUserDetailsService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

import static com.example.backendsalesdepartment.constants.SecurityConstants.*;
import static com.example.backendsalesdepartment.constants.UserRolesConstants.ADMIN;
import static com.example.backendsalesdepartment.constants.UserRolesConstants.CHIEF;


@EnableGlobalMethodSecurity(securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true)
@Configuration
@EnableWebSecurity
public class JWTSecurityConfig implements WebMvcConfigurer {
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenFilter jwtTokenFilter;

    public JWTSecurityConfig(CustomUserDetailsService customUserDetailsService,
                             JWTTokenFilter jwtTokenFilter) {
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenFilter = jwtTokenFilter;
    }


    @Override
    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**");
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
//                //пробовал закоментировать отлетает даже свагер
//              .cors(AbstractHttpConfigurer::disable)
//              .csrf(AbstractHttpConfigurer::disable)
                .cors().and().csrf().disable()
                //Настройка http запросов - кому куда можно/нельзя
                .authorizeHttpRequests(auth -> auth
                                .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(MANAGERS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(BOOKS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(AUTHORS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(REST.BOOKS_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(REST.AUTHORS_WHITE_LIST.toArray(String[]::new)).permitAll()
                                           .requestMatchers(USER_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, CHIEF)
//                                           .requestMatchers(AUTHORS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, LIBRARIAN)
//                                           .requestMatchers(BOOKS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, LIBRARIAN)
//                                           .requestMatchers(REST.BOOKS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, LIBRARIAN)
//                                           .requestMatchers(REST.USERS_PERMISSION_LIST.toArray(String[]::new)).hasRole(USER)
                                .anyRequest().authenticated()
                )
                .exceptionHandling()
                .authenticationEntryPoint((request, response, authException) -> {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                            authException.getMessage());
                })
                // .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .sessionManagement(
                        session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                //JWT Token Filter VALID or NOT
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .userDetailsService(customUserDetailsService)
                .build();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }
}




