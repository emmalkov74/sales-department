package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/clients")
@Tag(name = "Клиенты",
        description = "Контроллер для работы с контактными лицами контрагентов")
public class ClientController extends GenericController<Client, ClientDTO>{
    private ClientService clientService;
    public ClientController(ClientService clientService) {
        super(clientService);
        this.clientService = clientService;
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    @Operation(description = "Получить все записи по заданному id Компании", method = "getAllByCompanyId")
    @RequestMapping(value = "/getAllByCompanyId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ClientDTO>> getAllByCompanyId(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.getAllByCompanyID(id));
    }

}

