package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.DealDTO;
import com.example.backendsalesdepartment.models.Deal;
import com.example.backendsalesdepartment.service.DealService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/deals")
@Tag(name = "Сделки",
        description = "Контроллер для работы со сделками менеджеров")
public class DealController extends GenericController<Deal, DealDTO> {

    private final DealService dealService;

    public DealController(DealService dealService) {

        super(dealService);
        this.dealService = dealService;
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    @Operation(description = "Получить все сделки по заданному id Компании", method = "getAllByCompanyId")
    @RequestMapping(value = "/getAllByCompanyId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DealDTO>> getAllByCompanyId(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dealService.getAllByCompanyID(id));
    }
}
