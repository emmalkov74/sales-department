package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.config.jwt.JWTTokenUtil;
import com.example.backendsalesdepartment.dto.LoginDTO;
import com.example.backendsalesdepartment.dto.UserDTO;
import com.example.backendsalesdepartment.models.User;
import com.example.backendsalesdepartment.service.UserService;
import com.example.backendsalesdepartment.service.userdetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
@Tag(name = "Менеджеры",
        description = "Контроллер для работы с менеджерами компании")
@Slf4j
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*")

//localhost:9090/api/rest/users

public class UserController
        extends GenericController<User, UserDTO>
{
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;
    private final UserService userService;

    public UserController(UserService userService,
                          CustomUserDetailsService customUserDetailsService,
                          JWTTokenUtil jwtTokenUtil) {
        super(userService);
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.info("LoginDTO: {}", loginDTO);
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("user_role", foundUser.getAuthorities().stream().toArray()[0].toString());
        return ResponseEntity.ok().body(response);
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    //вернуть информацию о книге по переданному login
    @Operation(description = "Получить запись по login", method = "getOneByLogin")
    @RequestMapping(value = "/getOneByLogin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getOneByLogin(@RequestParam(value = "login") String login) {
        return ResponseEntity.status(HttpStatus.OK)
                // .body(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данных с переданным ID не найдено")));
                .body(userService.getOneByLogin(login));
    }
    @CrossOrigin(origins = "http://localhost:3000/")
    //вернуть информацию о книге по переданному email
    @Operation(description = "Получить запись по email", method = "getOneByEmail")
    @RequestMapping(value = "/getOneByEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getOneByEmail(@RequestParam(value = "email") String email) {
        return ResponseEntity.status(HttpStatus.OK)
                // .body(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данных с переданным ID не найдено")));
                .body(userService.getOneByEmail(email));
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    //вернуть информацию существует ли заданный  email
    @Operation(description = "Получить информацию существует ли заданный email", method = "getIsEmail")
    @RequestMapping(value = "/getIsEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> getIsEmail(@RequestParam(value = "email") String email) {

        return ResponseEntity.status(HttpStatus.OK)
                // .body(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данных с переданным ID не найдено")));
                .body(userService.isEmail(email));
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    //вернуть информацию что пароль изменен
    @Operation(description = "Получить информацию что пароль изменен", method = "changePassword")
    @RequestMapping(value = "/change-password", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> changePassword(@RequestBody LoginDTO loginDTO) {

        return ResponseEntity.status(HttpStatus.OK)
                // .body(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данных с переданным ID не найдено")));
                .body(userService.changePassword(loginDTO));
    }


}
