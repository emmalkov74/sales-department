package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.service.ActionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/actions")
@Tag(name = "Активности",
        description = "Контроллер для работы с активностями менеджеров")
public class ActionController extends GenericController<Action, ActionDTO> {
    private final ActionService actionService;
    public ActionController(ActionService actionService) {

        super(actionService);
        this.actionService = actionService;
    }
    @CrossOrigin(origins = "http://localhost:3000/")
    @Operation(description = "Получить все активности по заданному id Компании", method = "getAllByCompanyId")
    @RequestMapping(value = "/getAllByCompanyId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ActionDTO>> getAllByCompanyId(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(actionService.getAllByCompanyID(id));
    }
}
