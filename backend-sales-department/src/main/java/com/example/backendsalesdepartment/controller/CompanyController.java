package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.dto.CompanyDTO;
import com.example.backendsalesdepartment.dto.LoginDTO;
import com.example.backendsalesdepartment.models.Company;
import com.example.backendsalesdepartment.service.CompanyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping(value = "/companies")
@Tag(name = "Компании", description = "Контроллер для работы с Контрагентами")
public class CompanyController extends GenericController<Company, CompanyDTO> {

    private CompanyService companyService;
    public CompanyController(CompanyService companyService) {
        super(companyService);
        this.companyService = companyService;
    }

    @CrossOrigin(origins = "http://localhost:3000/")
    @Operation(description = "Получить нужные записи", method = "search")
    @RequestMapping(value = "/search", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CompanyDTO>> searchCompanies(@RequestBody CompanyDTO companyDTO){
        return ResponseEntity.status(HttpStatus.OK).body(companyService.search(companyDTO));
    }





}
