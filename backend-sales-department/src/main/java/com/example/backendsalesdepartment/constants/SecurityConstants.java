package com.example.backendsalesdepartment.constants;

import java.util.List;

public interface SecurityConstants {
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/js/**",
            "/css/**",
            "/",
            // -- Swagger UI v3 (OpenAPI)
            "/swagger-ui/**",
            "/webjars/bootstrap/5.0.2/**",
            "/v3/api-docs/**",
            "/error");
    List<String> USERS_WHITE_LIST = List.of(
            "/users",
            "/users/auth",
            "/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/users/getOneByLogin",
            "/users/getOneByEmail",
            "/users/getIsEmail"
    );

    List<String> MANAGERS_WHITE_LIST = List.of(
            "/companies",
            "/companies/getAll",
            "/clients",
            "/clients/add",
            "/actions/add",
            "/deals/add",
            "/companies/search",
            "/clients/getAllByCompanyId",
            "/deals/getAllByCompanyId",
            "/deals/update",
            "/actions/update",
            "/actions/getAllByCompanyId",
            "http://localhost:9090/api/rest",
            "/companies/getOneById"

            );

    List<String> USER_PERMISSION_LIST = List.of(
            "/users/add",
            "/users/delete",
            "/users/update"
    );



}
