package com.example.backendsalesdepartment.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String CHIEF = "CHIEF";
    String USER = "USER";

}
