package com.example.backendsalesdepartment.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "actions")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "companies_seq", allocationSize = 1)
public class Action extends GenericModel{
    private LocalDate actionDate;
    private String title;
    private String description;
    private Boolean isDone;

    //Как читать ManyToOne: у менеджера много активностей
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_ACTION_USER"))
    private User user;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_ACTION_COMPANY"))

    private Company company;

}
