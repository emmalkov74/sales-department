package com.example.backendsalesdepartment.models;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "deals")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "companies_seq", allocationSize = 1)
public class Deal extends GenericModel{
    private LocalDate dealDate;
    private Long dealAmount;
    private Boolean isDone;
    //Как читать ManyToOne: у менеджера много активностей
    @ManyToOne(cascade = { CascadeType.PERSIST})
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_DEAL_USER"))
    private User user;

    @ManyToOne(cascade = { CascadeType.PERSIST})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_DEAL_COMPANY"))
    private Company company;

}
