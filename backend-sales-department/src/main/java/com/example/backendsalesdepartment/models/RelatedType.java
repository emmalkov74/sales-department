package com.example.backendsalesdepartment.models;

public enum RelatedType {
    ACTION,
    CLIENT,
    DEAL,
    COMPANY,
    MANAGER
}
