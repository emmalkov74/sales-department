package com.example.backendsalesdepartment.models;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


//TODO: https://ondras.zarovi.cz/sql/demo/?keyword=default - онлайн рисовалка диаграмм
@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass

public class Person extends GenericModel {
    @Column(name = "fio", nullable = false)
    private String fio;
    @Column(name = "phone", nullable = false, unique = true)
    private String phone;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "position")
    private String position;

}
