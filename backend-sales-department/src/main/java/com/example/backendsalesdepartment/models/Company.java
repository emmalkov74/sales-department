package com.example.backendsalesdepartment.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "companies")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "companies_seq", allocationSize = 1)

public class Company extends GenericModel {
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "inn", nullable = false, unique = true)
    private String inn;

    @Column(name = "address")
    private String address;


    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Client> clients;

    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Deal> deals;

    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Action> actions;

}
