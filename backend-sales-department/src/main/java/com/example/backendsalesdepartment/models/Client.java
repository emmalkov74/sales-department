package com.example.backendsalesdepartment.models;
import jakarta.persistence.*;
import lombok.*;

import java.util.Set;


@Entity
@Table(name = "clients")
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "clients_seq", allocationSize = 1)

public class Client extends Person {

    //Как читать ManyToOne: у Компании много контактных лиц (Клиентов)

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_CLIENT_COMPANY"))
    private Company company;




}
