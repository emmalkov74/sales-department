package com.example.backendsalesdepartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendSalesDepartmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendSalesDepartmentApplication.class, args);
    }

}
