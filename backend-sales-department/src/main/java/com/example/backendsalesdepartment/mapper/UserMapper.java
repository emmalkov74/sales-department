package com.example.backendsalesdepartment.mapper;

import com.example.backendsalesdepartment.dto.UserDTO;
import com.example.backendsalesdepartment.models.GenericModel;
import com.example.backendsalesdepartment.models.User;
import com.example.backendsalesdepartment.models.RelatedType;
import com.example.backendsalesdepartment.repository.ActionRepository;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import com.example.backendsalesdepartment.repository.DealRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {
    private CompanyRepository companyRepository;
    private ActionRepository actionRepository;
    private DealRepository dealRepository;
    protected UserMapper(ModelMapper mapper,
                         CompanyRepository companyRepository,
                         ActionRepository actionRepository,
                         DealRepository dealRepository) {
        super(mapper, User.class, UserDTO.class);
        this.companyRepository = companyRepository;
        this.actionRepository = actionRepository;
       this.dealRepository = dealRepository;
    }

    @Override
    protected void setupMapper() {

        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> {
                    m.skip(UserDTO::setActionsIds);
                    m.skip(UserDTO::setDealsIds);
                }).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> {
                    m.skip(User::setActions);
                    m.skip(User::setDeals);
                }).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {


        if (!Objects.isNull(source.getActionsIds())) {
            destination.setActions(new HashSet<>(actionRepository.findAllById(source.getActionsIds())));
        } else {
            destination.setActions(Collections.emptySet());
        }

        if (!Objects.isNull(source.getDealsIds())) {
            destination.setDeals(new HashSet<>(dealRepository.findAllById(source.getDealsIds())));
        } else {
            destination.setDeals(Collections.emptySet());
        }

    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setActionsIds(getIds(source, RelatedType.ACTION));
        destination.setDealsIds(getIds(source, RelatedType.DEAL));
    }

    protected  Set<Long> getIds(User user, RelatedType relatedType) {

        if(!Objects.isNull(user)){

            if (relatedType == RelatedType.ACTION && !Objects.isNull(user.getActions())) {

                return user.getActions().stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toSet());
            }

            if (relatedType == RelatedType.DEAL && !Objects.isNull(user.getDeals())) {
                return user.getDeals().stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toSet());
            }
        }
        return null;
    }

}
