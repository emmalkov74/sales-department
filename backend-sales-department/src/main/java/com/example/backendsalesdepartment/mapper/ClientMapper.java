package com.example.backendsalesdepartment.mapper;

import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.models.GenericModel;
import com.example.backendsalesdepartment.models.RelatedType;
import com.example.backendsalesdepartment.repository.ActionRepository;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import com.example.backendsalesdepartment.service.CompanyService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ClientMapper extends GenericMapper<Client, ClientDTO> {

    private CompanyRepository companyRepository;


    public ClientMapper(ModelMapper mapper,
                        CompanyRepository companyRepository) {
        super(mapper, Client.class, ClientDTO.class);
        this.companyRepository = companyRepository;

    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Client.class, ClientDTO.class)
                .addMappings(m -> m.skip(ClientDTO::setCompanyId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(ClientDTO.class, Client.class)
                .addMappings(m -> m.skip(Client::setCompany)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ClientDTO source, Client destination) {
        destination.setCompany(companyRepository.findById(source.getCompanyId())
                .orElseThrow(() -> new NotFoundException("Компаниии не найдено")));
    }

    @Override
    protected void mapSpecificFields(Client source, ClientDTO destination) {
        destination.setCompanyId(source.getCompany().getId());
    }

    @Override
    protected Set<Long> getIds(Client client, RelatedType relatedType) {

        return null;
    }


}


