package com.example.backendsalesdepartment.mapper;
import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.dto.CompanyDTO;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.models.Company;
import com.example.backendsalesdepartment.models.GenericModel;
import com.example.backendsalesdepartment.models.RelatedType;
import com.example.backendsalesdepartment.repository.ActionRepository;
import com.example.backendsalesdepartment.repository.ClientRepository;
import com.example.backendsalesdepartment.repository.DealRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class CompanyMapper
        extends GenericMapper<Company, CompanyDTO> {
    private final ClientRepository clientRepository;
    private final DealRepository dealRepository;

    private ActionRepository actionRepository;

    protected CompanyMapper(ModelMapper mapper,
                            ClientRepository clientRepository,
                            DealRepository dealRepository,
                            ActionRepository actionRepository) {
        super(mapper, Company.class, CompanyDTO.class);
        this.clientRepository = clientRepository;
        this.dealRepository = dealRepository;
        this.actionRepository = actionRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Company.class, CompanyDTO.class)
                .addMappings(m -> m.skip(CompanyDTO::setActionsIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(CompanyDTO::setClientsIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(CompanyDTO::setDealsIds)).setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(CompanyDTO.class, Company.class)
                .addMappings(m -> m.skip(Company::setClients)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Company::setDeals)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Company::setActions)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(CompanyDTO source, Company destination) {
        if (!Objects.isNull(source.getActionsIds())) {
            destination.setActions(new HashSet<>(actionRepository.findAllById(source.getActionsIds())));
        } else {
            destination.setActions(Collections.emptySet());
        }

        if (!Objects.isNull(source.getClientsIds())) {
            destination.setClients(new HashSet<>(clientRepository.findAllById(source.getClientsIds())));
        }
        else {
            destination.setClients(Collections.emptySet());
        }

        if (!Objects.isNull(source.getDealsIds())) {
            destination.setDeals(new HashSet<>(dealRepository.findAllById(source.getDealsIds())));
        }
        else {
            destination.setDeals(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Company source, CompanyDTO destination) {
        destination.setClientsIds(getIds(source, RelatedType.CLIENT));
        destination.setDealsIds(getIds(source, RelatedType.DEAL));
        destination.setActionsIds(getIds(source, RelatedType.ACTION));
    }

    protected Set<Long> getIds(Company company, RelatedType relatedType) {
        if(!Objects.isNull(company)){

            if(relatedType == RelatedType.CLIENT && !Objects.isNull(company.getClients())){
                return company.getClients().stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toSet());
            }

            if (relatedType == RelatedType.ACTION && !Objects.isNull(company.getDeals())) {

                return company.getDeals().stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toSet());
            }

            if (relatedType == RelatedType.ACTION && !Objects.isNull(company.getActions())) {

                return company.getActions().stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toSet());
            }
        }
        return null;
    }

}


