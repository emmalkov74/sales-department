package com.example.backendsalesdepartment.mapper;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.models.RelatedType;
import com.example.backendsalesdepartment.repository.ActionRepository;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import com.example.backendsalesdepartment.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;


import java.util.Set;

@Component
public class ActionMapper extends GenericMapper<Action, ActionDTO>{

    private CompanyRepository companyRepository;
    private UserRepository userRepository;
    public ActionMapper(ModelMapper mapper,
                        CompanyRepository companyRepository,
                        UserRepository userRepository) {
        super(mapper, Action.class, ActionDTO.class);
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
    }
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Action.class, ActionDTO.class)
                .addMappings(m -> m.skip(ActionDTO::setCompanyId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(ActionDTO::setUserId));
        modelMapper.createTypeMap(ActionDTO.class, Action.class)
                .addMappings(m -> m.skip(Action::setCompany)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Action::setUser)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ActionDTO source, Action destination) {
        destination.setCompany(companyRepository.findById(source.getCompanyId())
                .orElseThrow(() -> new NotFoundException("Компаниии не найдено")));
        destination.setUser(userRepository.findById(source.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Action source, ActionDTO destination) {
        destination.setCompanyId(source.getCompany().getId());
        destination.setUserId(source.getUser().getId());
    }

    @Override
    protected Set<Long> getIds(Action client, RelatedType relatedType) {
        return null;
    }

}
