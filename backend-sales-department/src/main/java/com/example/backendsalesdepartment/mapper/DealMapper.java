package com.example.backendsalesdepartment.mapper;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.DealDTO;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.models.Deal;
import com.example.backendsalesdepartment.models.RelatedType;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import com.example.backendsalesdepartment.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;
@Component
public class DealMapper extends GenericMapper<Deal, DealDTO>{
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;

    public DealMapper(ModelMapper mapper,
                      CompanyRepository companyRepository,
                      UserRepository userRepository) {
        super(mapper, Deal.class, DealDTO.class);
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Deal.class, DealDTO.class)
                .addMappings(m -> m.skip(DealDTO::setCompanyId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(DealDTO::setUserId));
        modelMapper.createTypeMap(DealDTO.class, Deal.class)
                .addMappings(m -> m.skip(Deal::setCompany)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Deal::setUser)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DealDTO source, Deal destination) {
        destination.setCompany(companyRepository.findById(source.getCompanyId())
                .orElseThrow(() -> new NotFoundException("Компаниии не найдено")));
        destination.setUser(userRepository.findById(source.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Deal source, DealDTO destination) {
        destination.setCompanyId(source.getCompany().getId());
        destination.setUserId(source.getUser().getId());
    }

    @Override
    protected Set<Long> getIds(Deal entity, RelatedType relatedType) {
        return null;
    }


}
