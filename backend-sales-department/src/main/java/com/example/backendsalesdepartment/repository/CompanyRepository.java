package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends GenericRepository<Company> {

    @Query(nativeQuery = true,
            value = """
                select distinct c.*
                from companies c
                where c.name ilike '%' || btrim(coalesce(:name, c.name)) || '%'
                and c.inn ilike '%' || :inn || '%'
                and c.address ilike '%' || btrim(coalesce(:address, c.address)) || '%'
                and c.created_by ilike '%' || btrim(coalesce(:createdBy, c.created_by)) || '%'
            """)
    List<Company> searchCompanies(@Param(value = "name") String name,
                                  @Param(value = "inn") String inn,
                                  @Param(value = "address") String address,
                                  @Param(value = "createdBy") String createdBy);

}

