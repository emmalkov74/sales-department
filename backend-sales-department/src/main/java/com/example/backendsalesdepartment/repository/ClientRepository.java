package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.Client;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends PersonRepository<Client> {
    List<Client> findAllByCompanyId(final Long id);
}
