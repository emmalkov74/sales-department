package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.Person;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PersonRepository<M extends Person> extends GenericRepository<M>{
}
