package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.Action;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionRepository extends GenericRepository<Action>{
    List<Action> findAllByCompanyId(final Long id);
}
