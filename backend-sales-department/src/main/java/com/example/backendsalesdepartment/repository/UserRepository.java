package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PersonRepository<User> {
    //select * from users where login = ?
//    @Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String token);


}
