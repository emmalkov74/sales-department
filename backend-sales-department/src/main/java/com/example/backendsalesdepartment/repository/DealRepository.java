package com.example.backendsalesdepartment.repository;

import com.example.backendsalesdepartment.models.Deal;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRepository extends GenericRepository<Deal>{
    List<Deal> findAllByCompanyId(Long id);
}
