package com.example.backendsalesdepartment.dto;

import lombok.*;
import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActionDTO extends GenericDTO{
    private LocalDate actionDate;
    private String title;
    private String description;
    private Boolean isDone;
    private Long userId;
    private Long companyId;


}
