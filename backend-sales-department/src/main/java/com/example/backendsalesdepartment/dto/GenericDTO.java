package com.example.backendsalesdepartment.dto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@NoArgsConstructor

public abstract class GenericDTO {
    private Long id;
    private String createdBy;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDate createdWhen;
//  private boolean isDeleted;
//  private LocalDateTime deletedWhen;
//  private String deletedBy;

}
