package com.example.backendsalesdepartment.dto;
import lombok.*;
import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DealDTO extends GenericDTO{
    private LocalDate dealDate;
    private Long dealAmount;
    private Boolean isDone;
    private Long userId;
    private Long companyId;
}
