package com.example.backendsalesdepartment.dto;

import com.example.backendsalesdepartment.models.Deal;
import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO extends GenericDTO{
    private String name;
    private String inn;
    private String address;
    private Set<Long> clientsIds;
    private Set<Long> dealsIds;
    private Set<Long> actionsIds;
}
