package com.example.backendsalesdepartment.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDTO extends GenericDTO {
    private String fio;
    private String phone;
    private String email;
    private String position;
}
