package com.example.backendsalesdepartment.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDTO extends PersonDTO{
    private String login;
    private String password;
    private String changePasswordToken;
    private String role;
    private Set<Long> actionsIds;
    private Set<Long> dealsIds;

    public UserDTO(String fio, String phone, String email, String position, String login,
                   String password, String changePasswordToken, String role,
                   Set<Long> actionsIds, Set<Long> dealsIds) {
        super(fio, phone, email, position);
        this.login = login;
        this.password = password;
        this.changePasswordToken = changePasswordToken;
        this.role = role;
        this.actionsIds = actionsIds;
        this.dealsIds = dealsIds;
    }
}
