package com.example.backendsalesdepartment.dto;

import lombok.*;
import java.util.Set;
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
public class ClientDTO extends PersonDTO{
    private Long companyId;

    public ClientDTO(String fio, String phone, String email, String position, Long companyId) {
        super(fio, phone, email, position);
        this.companyId = companyId;
    }
}
