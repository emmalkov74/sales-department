package com.example.backendsalesdepartment.exception;

public class MyDeleteException
        extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
