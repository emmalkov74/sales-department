package com.example.backendsalesdepartment;

import com.example.backendsalesdepartment.dto.DealDTO;
import com.example.backendsalesdepartment.models.Deal;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.example.backendsalesdepartment.CompanyTestData.*;
import static com.example.backendsalesdepartment.UserTestData.*;


public interface DealTestData {
    DealDTO DEAL_DTO_1 = new DealDTO(
            LocalDate.now(),
            1L,
            false,
            1L,
            1L
    );
    DealDTO DEAL_DTO_2 = new DealDTO(
            LocalDate.now(),
            2L,
            false,
            2L,
            2L
    );
    DealDTO DEAL_DTO_3 = new DealDTO(
            LocalDate.now(),
            3L,
            false,
            3L,
            3L
    );
    List<DealDTO> DEAL_DTOS = Arrays.asList(DEAL_DTO_1, DEAL_DTO_2, DEAL_DTO_3);

    Deal DEAL_1 = new Deal(
            LocalDate.now(),
            1L,
            false,
            USER_1,
            COMPANY_1
    );
    Deal DEAL_2 = new Deal(
            LocalDate.now(),
            2L,
            false,
            USER_2,
            COMPANY_2
    );
    Deal DEAL_3 = new Deal(
            LocalDate.now(),
            3L,
            false,
            USER_3,
            COMPANY_3
    );
    List<Deal> DEAL_LIST = Arrays.asList(DEAL_1, DEAL_2, DEAL_3);
}
