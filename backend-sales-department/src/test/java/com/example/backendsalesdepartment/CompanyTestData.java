package com.example.backendsalesdepartment;

import com.example.backendsalesdepartment.dto.CompanyDTO;
import com.example.backendsalesdepartment.models.Company;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.example.backendsalesdepartment.UserTestData.*;


public interface CompanyTestData {
    CompanyDTO COMPANY_DTO_1 = new CompanyDTO(
            "name",
            "544",
            "Moscow",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()

    );
    CompanyDTO COMPANY_DTO_2 = new CompanyDTO(
            "name_1",
            "5442",
            "Moscow",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()

    );
    CompanyDTO COMPANY_DTO_3 = new CompanyDTO(
            "name_3",
            "5443",
            "Moscow",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()

    );

    List<CompanyDTO> COMPANY_DTO_LIST = Arrays.asList(COMPANY_DTO_1, COMPANY_DTO_2, COMPANY_DTO_3);

    Company COMPANY_1 = new Company(
            "Lapa1",
            "54311",
            "Narva",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()
    );
    Company COMPANY_2 = new Company(
            "Lapa2",
            "543112",
            "Narva2",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()
    );
    Company COMPANY_3 = new Company(
            "Lapa3",
            "543113",
            "Narva3",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>()
    );

    List<Company> COMPANY_LIST = Arrays.asList(COMPANY_1, COMPANY_2, COMPANY_3);

}
