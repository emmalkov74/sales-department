package com.example.backendsalesdepartment;

import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.models.Company;

import java.util.Arrays;
import java.util.List;

import static com.example.backendsalesdepartment.ActionTestData.ACTION_LIST;
import static com.example.backendsalesdepartment.CompanyTestData.*;

public interface ClientTestData {
    ClientDTO CLIENT_DTO_1 = new ClientDTO(
            "fio1",
            "phone1",
            "email1",
            "position1",
            1L
    );
    ClientDTO CLIENT_DTO_2 = new ClientDTO(
            "fio2",
            "phone2",
            "email2",
            "position2",
            2L
    );
    ClientDTO CLIENT_DTO_3 = new ClientDTO(
            "fio3",
            "phone3",
            "email3",
            "position3",
            3L
    );

    List<ClientDTO> CLIENT_DTO_LIST = Arrays.asList(CLIENT_DTO_1, CLIENT_DTO_2, CLIENT_DTO_3);

    Client CLIENT_1 = new Client(
            new Company()
    );
    Client CLIENT_2 = new Client(
            COMPANY_2
    );Client CLIENT_3 = new Client(
            COMPANY_3
    );

    List<Client> CLIENT_LIST = Arrays.asList(CLIENT_1, CLIENT_2, CLIENT_3);
}
