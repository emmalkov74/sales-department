package com.example.backendsalesdepartment;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.models.Company;
import com.example.backendsalesdepartment.models.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.example.backendsalesdepartment.UserTestData.*;


public interface ActionTestData {
    ActionDTO ACTION_DTO_1 = new ActionDTO(
            LocalDate.now(),
            "",
            "",
            false,
            1L,
            1L
    );
    ActionDTO ACTION_DTO_2 = new ActionDTO(
            LocalDate.now(),
            "",
            "",
            false,
            2L,
            2L
    );
    ActionDTO ACTION_DTO_3 = new ActionDTO(
            LocalDate.now(),
            "",
            "",
            false,
            3L,
            3L
    );

    List<ActionDTO> ACTION_DTO_LIST = Arrays.asList(ACTION_DTO_1, ACTION_DTO_2, ACTION_DTO_3);

    Action ACTION_1 = new Action(
            LocalDate.now(),
            "",
            "",
            false,
            new User(),
            new Company()
    );
    Action ACTION_2 = new Action(
            LocalDate.now(),
            "",
            "",
            false,
            new User(),
            new Company()
    );
    Action ACTION_3 = new Action(
            LocalDate.now(),
            "",
            "",
            false,
            USER_3,
            new Company()
    );
    List<Action> ACTION_LIST = Arrays.asList(ACTION_1, ACTION_2, ACTION_3);
}
