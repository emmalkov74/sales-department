package com.example.backendsalesdepartment.controller;

import com.example.backendsalesdepartment.CompanyTestData;
import com.example.backendsalesdepartment.dto.CompanyDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class CompanyRestControllerTest extends CommonTest {
    private static Long createdCompanyID;
    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех компаний через REST начат успешно");
        String result = mvc.perform(
                        get("/companies/getAll")
                                .headers(headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<CompanyDTO> companyDTOS = objectMapper.readValue(result, new TypeReference<List<CompanyDTO>>() {});
        companyDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех компаний через REST закончен успешно");

    }
    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию компании через REST начат успешно");
        //Создаем новой компании для создания через контроллер (тест дата)


        CompanyDTO companyDTO = CompanyTestData.COMPANY_DTO_1;
//        "REST_TestUserLogin", "REST_TestUserPassword","","REST_TestUserRole",  new HashSet<>(), new HashSet<>()
        /*
        Вызываем метод создания (POST) в контроллере, передаем ссылку на REST API в MOCK.
        В headers передаем токен для авторизации (под админом, смотри родительский класс).
        Ожидаем, что статус ответа будет успешным и что в ответе есть поле ID, а далее возвращаем контент как строку
        Все это мы конвертируем в AuthorDTO при помощи ObjectMapper от библиотеки Jackson.
        Присваиваем в статическое поле ID созданного автора, чтобы далее с ним работать.
         */
        CompanyDTO result = objectMapper.readValue(mvc.perform(post("/companies/add")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .content(asJsonString(companyDTO))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                CompanyDTO.class);
        createdCompanyID = result.getId();
        log.info("Тест по созданию компании через REST закончен успешно " + result);
        /*
        можно запустить один тест и по цепочке вызывать остальные:
        updateAuthor(createdAuthorID);
         */

    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению компании через REST начат успешно");
        //получаем нашего автора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        CompanyDTO existingCompany = objectMapper.readValue(mvc.perform(get("/companies/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdCompanyID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                CompanyDTO.class);
        //обновляем поля
        existingCompany.setName("REST_TestCompanyNameUPDATED");
        existingCompany.setAddress("Test Position UPDATED");

        //вызываем update через REST API
        mvc.perform(put("/companies/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(super.headers)
                        .content(asJsonString(existingCompany))
                        .param("id", String.valueOf(createdCompanyID))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления компании через REST закончен успешно");

    }

    @Test
    @Order(3)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению компании через REST начат успешно");
        mvc.perform(delete("/companies/delete/{id}", createdCompanyID)
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по удалению компании через REST завершен успешно");
        log.info("Данные очищены");
    }

}