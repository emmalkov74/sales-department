package com.example.backendsalesdepartment.controller;


import com.example.backendsalesdepartment.ClientTestData;
import com.example.backendsalesdepartment.dto.ClientDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class ClientRestControllerTest extends CommonTest {
    private static Long createdClientID;
    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех контактных лиц через REST начат успешно");
        String result = mvc.perform(
                        get("/clients/getAll")
                                .headers(headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<ClientDTO> clientDTOS = objectMapper.readValue(result, new TypeReference<List<ClientDTO>>() {});
        clientDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех контактных лиц через REST закончен успешно");

    }
    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию контактного лица через REST начат успешно");
        //Создаем нового автора для создания через контроллер (тест дата)


        ClientDTO clientDTO = ClientTestData.CLIENT_DTO_1;
//        "REST_TestUserLogin", "REST_TestUserPassword","","REST_TestUserRole",  new HashSet<>(), new HashSet<>()
        /*
        Вызываем метод создания (POST) в контроллере, передаем ссылку на REST API в MOCK.
        В headers передаем токен для авторизации (под админом, смотри родительский класс).
        Ожидаем, что статус ответа будет успешным и что в ответе есть поле ID, а далее возвращаем контент как строку
        Все это мы конвертируем в AuthorDTO при помощи ObjectMapper от библиотеки Jackson.
        Присваиваем в статическое поле ID созданного автора, чтобы далее с ним работать.
         */
        ClientDTO result = objectMapper.readValue(mvc.perform(post("/clients/add")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .content(asJsonString(clientDTO))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                ClientDTO.class);
        createdClientID = result.getId();
        log.info("Тест по созданию контактного лица через REST закончен успешно " + result);
        /*
        можно запустить один тест и по цепочке вызывать остальные:
        updateAuthor(createdAuthorID);
         */

    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению контактного лица через REST начат успешно");
        //получаем нашего автора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        ClientDTO existingUser = objectMapper.readValue(mvc.perform(get("/clients/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdClientID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                ClientDTO.class);
        //обновляем поля
        existingUser.setFio("REST_TestAuthorFioUPDATED");
        existingUser.setPosition("Test Position UPDATED");

        //вызываем update через REST API
        mvc.perform(put("/clients/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(super.headers)
                        .content(asJsonString(existingUser))
                        .param("id", String.valueOf(createdClientID))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления контактного лица через REST закончен успешно");

    }


    @Test
    @Order(3)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению контактного лица через REST начат успешно");
        mvc.perform(delete("/clients/delete/{id}", createdClientID)
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по удалению контактного лица  через REST завершен успешно");
        log.info("Данные очищены");
    }

}