package com.example.backendsalesdepartment.controller;


import com.example.backendsalesdepartment.ActionTestData;
import com.example.backendsalesdepartment.ClientTestData;
import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.ClientDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
public class ActionRestControllerTest extends CommonTest {
    private static Long createdActionID;
    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех активностей через REST начат успешно");
        String result = mvc.perform(
                        get("/actions/getAll")
                                .headers(headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<ActionDTO> actionDTOS = objectMapper.readValue(result, new TypeReference<List<ActionDTO>>() {});
        actionDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех активностей через REST закончен успешно");

    }
    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию активности через REST начат успешно");
        //Создаем нового автора для создания через контроллер (тест дата)


        ActionDTO actionDTO = ActionTestData.ACTION_DTO_1;
//        "REST_TestUserLogin", "REST_TestUserPassword","","REST_TestUserRole",  new HashSet<>(), new HashSet<>()
        /*
        Вызываем метод создания (POST) в контроллере, передаем ссылку на REST API в MOCK.
        В headers передаем токен для авторизации (под админом, смотри родительский класс).
        Ожидаем, что статус ответа будет успешным и что в ответе есть поле ID, а далее возвращаем контент как строку
        Все это мы конвертируем в AuthorDTO при помощи ObjectMapper от библиотеки Jackson.
        Присваиваем в статическое поле ID созданного автора, чтобы далее с ним работать.
         */
        ActionDTO result = objectMapper.readValue(mvc.perform(post("/actions/add")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .content(asJsonString(actionDTO))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                ActionDTO.class);
        createdActionID = result.getId();
        log.info("Тест по созданию активности через REST закончен успешно " + result);
        /*
        можно запустить один тест и по цепочке вызывать остальные:
        updateAuthor(createdAuthorID);
         */

    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению активности через REST начат успешно");
        //получаем нашего автора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        ActionDTO existingAction = objectMapper.readValue(mvc.perform(get("/actions/getOneById")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdActionID))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                ActionDTO.class);
        //обновляем поля
        existingAction.setTitle("REST_TestAuthorFioUPDATED");
        existingAction.setDescription("Test Position UPDATED");

        //вызываем update через REST API
        mvc.perform(put("/actions/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(super.headers)
                        .content(asJsonString(existingAction))
                        .param("id", String.valueOf(createdActionID))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления активности через REST закончен успешно");

    }


    @Test
    @Order(3)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению активности через REST начат успешно");
        mvc.perform(delete("/actions/delete/{id}", createdActionID)
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Тест по удалению активности через REST завершен успешно");
        log.info("Данные очищены");
    }

}