package com.example.backendsalesdepartment;

import com.example.backendsalesdepartment.dto.UserDTO;
import com.example.backendsalesdepartment.models.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public interface UserTestData {

    UserDTO USER_DTO_1 = new UserDTO(
            "fio",
            "phone",
            "email",
            "position",
            "login1",
            "password1",
             "",
             "USER",
            new HashSet<>(),
            new HashSet<>());

    UserDTO USER_DTO_2 = new UserDTO(
            "fio",
            "phone",
            "email",
            "position",
            "login2",
            "password2",
            "",
            "USER2",
            new HashSet<>(),
            new HashSet<>());

    UserDTO USER_DTO_3 = new UserDTO(
            "fio",
            "phone",
            "email",
            "position",
            "login3",
            "password3",
            "",
            "USER3",
            new HashSet<>(),
            new HashSet<>());


    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO_1, USER_DTO_2, USER_DTO_3);

    User USER_1 = new User(
            "login1",
            "password1",
            "changePasswordToken1",
            "USER",
            new HashSet<>(),
            new HashSet<>());
    User USER_2 = new User(
            "login2",
            "password2",
            "changePasswordToken2",
            "USER",
            new HashSet<>(),
            new HashSet<>());
    User USER_3 = new User(
            "login3",
            "password3",
            "changePasswordToken3",
            "USER",
            new HashSet<>(),
            new HashSet<>());

    List<User> USER_LIST = List.of(USER_1, USER_2, USER_3);

}
