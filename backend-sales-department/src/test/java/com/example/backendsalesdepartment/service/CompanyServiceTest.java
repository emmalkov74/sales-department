package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.CompanyDTO;
import com.example.backendsalesdepartment.exception.MyDeleteException;
import com.example.backendsalesdepartment.mapper.CompanyMapper;
import com.example.backendsalesdepartment.models.Company;
import com.example.backendsalesdepartment.repository.CompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Optional;
import static com.example.backendsalesdepartment.CompanyTestData.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class CompanyServiceTest extends GenericTest<Company, CompanyDTO>{
    public CompanyServiceTest() {
        super();
        repository = Mockito.mock(CompanyRepository.class);
        mapper = Mockito.mock(CompanyMapper.class);
        service = new CompanyService((CompanyRepository) repository, (CompanyMapper) mapper);

    }

    @Test
    @Order(1)
    @Override
    public void getAll() {
        Mockito.when(repository.findAll()).thenReturn(COMPANY_LIST);
        Mockito.when(mapper.toDTOs(COMPANY_LIST)).thenReturn(COMPANY_DTO_LIST);
        List<CompanyDTO> companyDTOS = service.listAll();
        log.info("Testing getAll(): " + companyDTOS);
        assertEquals(COMPANY_LIST.size(), companyDTOS.size());

    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(COMPANY_1));
        Mockito.when(mapper.toDto(COMPANY_1)).thenReturn(COMPANY_DTO_1);
        CompanyDTO companyDTO = service.getOne(1L);
        log.info("Testing getOne(): " + companyDTO);
        assertEquals(COMPANY_DTO_1, companyDTO);
    }
    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(COMPANY_DTO_1)).thenReturn(COMPANY_1);
        Mockito.when(mapper.toDto(COMPANY_1)).thenReturn(COMPANY_DTO_1);
        Mockito.when(repository.save(COMPANY_1)).thenReturn(COMPANY_1);
        CompanyDTO companyDTO = service.create(COMPANY_DTO_1);
        log.info("Testing create(): " + companyDTO);
        assertEquals(COMPANY_DTO_1, companyDTO);
    }
    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(COMPANY_DTO_1)).thenReturn(COMPANY_1);
        Mockito.when(mapper.toDto(COMPANY_1)).thenReturn(COMPANY_DTO_1);
        Mockito.when(repository.save(COMPANY_1)).thenReturn(COMPANY_1);
        CompanyDTO companyDTO = service.update(COMPANY_DTO_1);
        log.info("Testing update(): " + companyDTO);
        assertEquals(COMPANY_DTO_1, companyDTO);
    }


    @Override
    protected void delete() throws MyDeleteException {

    }

    @Override
    protected void restore() {

    }

    @Override
    protected void getAllNotDeleted() {

    }
}
