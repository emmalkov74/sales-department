package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.exception.MyDeleteException;
import com.example.backendsalesdepartment.mapper.ActionMapper;
import com.example.backendsalesdepartment.models.Action;
import com.example.backendsalesdepartment.repository.ActionRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static com.example.backendsalesdepartment.ActionTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class ActionServiceTest extends GenericTest<Action, ActionDTO>{
    public ActionServiceTest() {
        super();
        repository = Mockito.mock(ActionRepository.class);
        mapper = Mockito.mock(ActionMapper.class);
        service = new ActionService((ActionRepository) repository, (ActionMapper) mapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(ACTION_LIST);
        Mockito.when(mapper.toDTOs(ACTION_LIST)).thenReturn(ACTION_DTO_LIST);
        List<ActionDTO> actionDTOS = service.listAll();
        log.info("Testing getAll(): " + actionDTOS);
        assertEquals(ACTION_LIST.size(), actionDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(ACTION_1));
        Mockito.when(mapper.toDto(ACTION_1)).thenReturn(ACTION_DTO_1);
        ActionDTO actionDTO = service.getOne(1L);
        log.info("Testing getOne(): " + actionDTO);
        assertEquals(ACTION_DTO_1, actionDTO);
    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(ACTION_DTO_1)).thenReturn(ACTION_1);
        Mockito.when(mapper.toDto(ACTION_1)).thenReturn(ACTION_DTO_1);
        Mockito.when(repository.save(ACTION_1)).thenReturn(ACTION_1);
        ActionDTO actionDTO = service.create(ACTION_DTO_1);
        log.info("Testing create(): " + actionDTO);
        assertEquals(ACTION_DTO_1, actionDTO);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(ACTION_DTO_1)).thenReturn(ACTION_1);
        Mockito.when(mapper.toDto(ACTION_1)).thenReturn(ACTION_DTO_1);
        Mockito.when(repository.save(ACTION_1)).thenReturn(ACTION_1);
        ActionDTO actionDTO = service.update(ACTION_DTO_1);
        log.info("Testing update(): " + actionDTO);
        assertEquals(ACTION_DTO_1, actionDTO);
    }


    @Override
    protected void delete() throws MyDeleteException {

    }

    @Override
    protected void restore() {

    }

    @Override
    protected void getAllNotDeleted() {

    }
}
