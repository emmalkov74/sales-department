package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.ClientDTO;
import com.example.backendsalesdepartment.exception.MyDeleteException;
import com.example.backendsalesdepartment.mapper.ClientMapper;
import com.example.backendsalesdepartment.models.Client;
import com.example.backendsalesdepartment.repository.ClientRepository;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static com.example.backendsalesdepartment.ClientTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class ClientServiceTest extends GenericTest<Client, ClientDTO> {
    public ClientServiceTest() {
        super();
        repository = Mockito.mock(ClientRepository.class);
        mapper = Mockito.mock(ClientMapper.class);
        service = new ClientService((ClientRepository) repository, (ClientMapper) mapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(CLIENT_LIST);
        Mockito.when(mapper.toDTOs(CLIENT_LIST)).thenReturn(CLIENT_DTO_LIST);
        List<ClientDTO> clientDTOS = service.listAll();
        log.info("Testing getAll(): " + clientDTOS);
        assertEquals(CLIENT_LIST.size(), clientDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        Mockito.when(mapper.toDto(CLIENT_1)).thenReturn(CLIENT_DTO_1);
        ClientDTO clientDTO = service.getOne(1L);
        log.info("Testing getOne(): " + clientDTO);
        assertEquals(CLIENT_DTO_1, clientDTO);
    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(CLIENT_DTO_1)).thenReturn(CLIENT_1);
        Mockito.when(mapper.toDto(CLIENT_1)).thenReturn(CLIENT_DTO_1);
        Mockito.when(repository.save(CLIENT_1)).thenReturn(CLIENT_1);
        ClientDTO clientDTO = service.create(CLIENT_DTO_1);
        log.info("Testing create(): " + clientDTO);
        assertEquals(CLIENT_DTO_1, clientDTO);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(CLIENT_DTO_1)).thenReturn(CLIENT_1);
        Mockito.when(mapper.toDto(CLIENT_1)).thenReturn(CLIENT_DTO_1);
        Mockito.when(repository.save(CLIENT_1)).thenReturn(CLIENT_1);
        ClientDTO clientDTO = service.update(CLIENT_DTO_1);
        log.info("Testing update(): " + clientDTO);
        assertEquals(CLIENT_DTO_1, clientDTO);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() throws MyDeleteException {
//        Mockito.when(((ClientRepository) repository).checkCl(1L)).thenReturn(true);
////        Mockito.when(authorRepository.checkAuthorForDeletion(2L)).thenReturn(false);
//        Mockito.when(repository.save(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_1);
//        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AuthorTestData.AUTHOR_1));
//        log.info("Testing delete() before: " + AuthorTestData.AUTHOR_1.isDeleted());
//        service.delete(1L);
//        log.info("Testing delete() after: " + AuthorTestData.AUTHOR_1.isDeleted());
//        assertTrue(AuthorTestData.AUTHOR_1.isDeleted());
    }



    @Override
    protected void restore() {

    }

    @Override
    protected void getAllNotDeleted() {

    }
}
