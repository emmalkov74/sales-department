package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.UserDTO;
import com.example.backendsalesdepartment.exception.MyDeleteException;
import com.example.backendsalesdepartment.mapper.UserMapper;
import com.example.backendsalesdepartment.models.User;
import com.example.backendsalesdepartment.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Optional;

import static com.example.backendsalesdepartment.UserTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class UserServiceTest extends GenericTest<User, UserDTO>{
    public UserServiceTest(BCryptPasswordEncoder bCryptPasswordEncoder, JavaMailSender javaMailSender) {
        super();
        repository = Mockito.mock(UserRepository.class);
        mapper = Mockito.mock(UserMapper.class);
        service = new UserService((UserRepository) repository, bCryptPasswordEncoder, (UserMapper) mapper, javaMailSender );
    }
    @Test
    @Order(1)
    @Override
    public void getAll() {
        Mockito.when(repository.findAll()).thenReturn(USER_LIST);
        Mockito.when(mapper.toDTOs(USER_LIST)).thenReturn(USER_DTO_LIST);
        List<UserDTO> userDTOS = service.listAll();
        log.info("Testing getAll(): " + userDTOS);
        assertEquals(USER_LIST.size(), userDTOS.size());

    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(USER_1));
        Mockito.when(mapper.toDto(USER_1)).thenReturn(USER_DTO_1);
        UserDTO userDTO = service.getOne(1L);
        log.info("Testing getOne(): " + userDTO);
        assertEquals(USER_DTO_1, userDTO);
    }
    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(USER_DTO_1)).thenReturn(USER_1);
        Mockito.when(mapper.toDto(USER_1)).thenReturn(USER_DTO_1);
        Mockito.when(repository.save(USER_1)).thenReturn(USER_1);
        UserDTO userDTO = service.create(USER_DTO_1);
        log.info("Testing create(): " + userDTO);
        assertEquals(USER_DTO_1, userDTO);
    }
    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(USER_DTO_1)).thenReturn(USER_1);
        Mockito.when(mapper.toDto(USER_1)).thenReturn(USER_DTO_1);
        Mockito.when(repository.save(USER_1)).thenReturn(USER_1);
        UserDTO userDTO = service.update(USER_DTO_1);
        log.info("Testing update(): " + userDTO);
        assertEquals(USER_DTO_1, userDTO);
    }


    @Order(5)
    @Test
    @Override
    protected void delete() throws MyDeleteException {

    }



    @Override
    protected void restore() {

    }

    @Override
    protected void getAllNotDeleted() {

    }
}
