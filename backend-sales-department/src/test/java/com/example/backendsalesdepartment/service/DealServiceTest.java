package com.example.backendsalesdepartment.service;

import com.example.backendsalesdepartment.dto.ActionDTO;
import com.example.backendsalesdepartment.dto.DealDTO;
import com.example.backendsalesdepartment.exception.MyDeleteException;
import com.example.backendsalesdepartment.mapper.DealMapper;
import com.example.backendsalesdepartment.models.Deal;
import com.example.backendsalesdepartment.repository.DealRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static com.example.backendsalesdepartment.ActionTestData.*;
import static com.example.backendsalesdepartment.ActionTestData.ACTION_DTO_1;
import static com.example.backendsalesdepartment.DealTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DealServiceTest extends GenericTest<Deal, DealDTO>{
    public DealServiceTest() {
        super();
        repository = Mockito.mock(DealRepository.class);
        mapper = Mockito.mock(DealMapper.class);
        service = new DealService((DealRepository) repository, (DealMapper) mapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DEAL_LIST);
        Mockito.when(mapper.toDTOs(DEAL_LIST)).thenReturn(DEAL_DTOS);
        List<DealDTO> dealDTOS = service.listAll();
        log.info("Testing getAll(): " + dealDTOS);
        assertEquals(DEAL_LIST.size(), dealDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DEAL_1));
        Mockito.when(mapper.toDto(DEAL_1)).thenReturn(DEAL_DTO_1);
        DealDTO dealDTO = service.getOne(1L);
        log.info("Testing getOne(): " + dealDTO);
        assertEquals(DEAL_DTO_1, dealDTO);
    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DEAL_DTO_1)).thenReturn(DEAL_1);
        Mockito.when(mapper.toDto(DEAL_1)).thenReturn(DEAL_DTO_1);
        Mockito.when(repository.save(DEAL_1)).thenReturn(DEAL_1);
        DealDTO dealDTO = service.create(DEAL_DTO_1);
        log.info("Testing create(): " + dealDTO);
        assertEquals(DEAL_DTO_1, dealDTO);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DEAL_DTO_1)).thenReturn(DEAL_1);
        Mockito.when(mapper.toDto(DEAL_1)).thenReturn(DEAL_DTO_1);
        Mockito.when(repository.save(DEAL_1)).thenReturn(DEAL_1);
        DealDTO dealDTO = service.update(DEAL_DTO_1);
        log.info("Testing update(): " + dealDTO);
        assertEquals(DEAL_DTO_1, dealDTO);
    }


    @Override
    protected void delete() throws MyDeleteException {

    }

    @Override
    protected void restore() {

    }

    @Override
    protected void getAllNotDeleted() {

    }
}
